package com.integrimarine.integrimediaplayer.Fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.util.Util;
import com.integrimarine.integriclient.Database.DatabaseSingleton;
import com.integrimarine.integrimediaplayer.Activities.TvScreenActivity;
import com.integrimarine.integrimediaplayer.Database.Models.CUMediaFileModel;
import com.integrimarine.integrimediaplayer.R;
import com.integrimarine.integrimediaplayer.Utils.Utils;

import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

/**
 * A simple {@link Fragment} subclass.
 */
public class ImageFragment extends Fragment {


    private String mImageUrl;
    private Timer timer;
    private ImageView imageView;
    private RelativeLayout image_fragment_placeholder;
    public ImageFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_image, container, false);
        imageView=rootView.findViewById( R.id.image_fragment_imageview );
        image_fragment_placeholder=rootView.findViewById( R.id.image_fragment_placeholder );
        timer = new Timer();
        getImage();
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                getImage();
            }
        }, 0, 1000 * 60);
        return rootView;
    }

    private void getImage() {
        Date date = new Date();
        CUMediaFileModel cuMediaFileModel = DatabaseSingleton.getInstance(getContext()).getDatabaseInterface().cu_media_screen_schedules().findMediaScheduleByScreenType("screen_one", date.getTime());
        if (cuMediaFileModel != null) {
            String imageurl = cuMediaFileModel.getMedia_name();
            if (!imageurl.equals(mImageUrl)) {
                mImageUrl = imageurl;
                initImageView();
            }
        } else {
            image_fragment_placeholder.setVisibility( View.VISIBLE );
        }
    }

    private void initImageView() {
        Glide.with( this ).load( Utils._BASE_PATH+"/"+mImageUrl )
        .into( imageView )
        ;

    }

}
