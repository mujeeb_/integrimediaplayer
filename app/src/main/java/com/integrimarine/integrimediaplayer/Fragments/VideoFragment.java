package com.integrimarine.integrimediaplayer.Fragments;


import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.VideoView;

import com.integrimarine.integriclient.Database.DatabaseSingleton;
import com.integrimarine.integrimediaplayer.Activities.TvScreenActivity;
import com.integrimarine.integrimediaplayer.Database.Models.CUMediaFileModel;
import com.integrimarine.integrimediaplayer.R;
import com.integrimarine.integrimediaplayer.Utils.Utils;

import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

/**
 * A simple {@link Fragment} subclass.
 */
public class VideoFragment extends Fragment {


    private String mVideoUrl;
    private VideoView videoView;
    private RelativeLayout video_fragment_placeholder;
    public VideoFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        timer = new Timer();
        View rootView = inflater.inflate(R.layout.fragment_video, container, false);
        videoView = rootView.findViewById(R.id.VideoView);
        video_fragment_placeholder=rootView.findViewById( R.id.video_fragment_placeholder );
        getVideo();
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                getVideo();
            }
        }, 0, 1000 * 60);
        return rootView;
    }

    public void getVideo() {
        Date date = new Date();
        Log.e( "getVideo: ",date.getTime() + "" );
        CUMediaFileModel cuMediaFileModel = DatabaseSingleton.getInstance(getContext()).getDatabaseInterface().cu_media_screen_schedules().findMediaScheduleByScreenType("screen_two", date.getTime());
        if (cuMediaFileModel != null) {
            String videourl = cuMediaFileModel.getMedia_name();
            if (!videourl.equals(mVideoUrl)) {
                mVideoUrl = videourl;
                initVideoPlayer();
            }
        }else{
            video_fragment_placeholder.setVisibility( View.VISIBLE );
        }
    }

    private Timer timer;

    private void initVideoPlayer() {
        videoView.setVideoURI( Uri.parse(Utils._BASE_PATH + "/" + mVideoUrl) );
        videoView.start();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        timer.cancel();
    }
}
