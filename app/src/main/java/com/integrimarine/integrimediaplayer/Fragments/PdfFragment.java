package com.integrimarine.integrimediaplayer.Fragments;


import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.github.barteksc.pdfviewer.PDFView;
import com.integrimarine.integriclient.Database.DatabaseSingleton;
import com.integrimarine.integrimediaplayer.Activities.TvScreenActivity;
import com.integrimarine.integrimediaplayer.Database.Models.CUMediaFileModel;
import com.integrimarine.integrimediaplayer.R;
import com.integrimarine.integrimediaplayer.Utils.Utils;
import com.itextpdf.text.pdf.PdfReader;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

/**
 * A simple {@link Fragment} subclass.
 */
public class PdfFragment extends Fragment {

    PDFView pdfView;
    int numberOfPages, i;
    Timer mTimer;
    private String mPdfUrl;
    private Timer timer;
    private RelativeLayout pdf_fragment_placeholder;

    public PdfFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        timer = new Timer();
        View view = inflater.inflate(R.layout.fragment_pdf, container, false);
        HandleTimer();
        pdfView = view.findViewById(R.id.pdfViewer);
        pdf_fragment_placeholder=view.findViewById( R.id.pdf_fragment_placeholder );

        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                getPdf();
            }
        }, 0, 1000 * 60);
        getPdf();
        return view;

    }

    private int mCurrentPage = 0;

    private void HandleTimer() {
        new Timer().scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                if (mCurrentPage < numberOfPages) {
                    mCurrentPage++;
                } else {
                    mCurrentPage = 0;
                }
            }
        }, 0, 3000);
    }

    private void getPdf() {
        Date date = new Date();
        CUMediaFileModel cuMediaFileModel = DatabaseSingleton.getInstance(getContext()).getDatabaseInterface().cu_media_screen_schedules().findMediaScheduleByScreenType("screen_three", date.getTime());
        if(cuMediaFileModel != null) {
            String pdfurl = cuMediaFileModel.getMedia_name();
            if (!pdfurl.equals(mPdfUrl)) {
                mPdfUrl = pdfurl;
                initPdfPlayer();
            }
        }else{
            pdf_fragment_placeholder.setVisibility( View.VISIBLE );
        }
    }

    private void initPdfPlayer() {
        try {
            PdfReader pdfReader = new PdfReader(Utils._BASE_PATH + "/"+ mPdfUrl);
            numberOfPages = pdfReader.getNumberOfPages();
        } catch (IOException e) {
            e.printStackTrace();
        }
        mCurrentPage = 0;
        pdfView.fromFile(new File(Utils._BASE_PATH + "/"+ mPdfUrl))
                .autoSpacing(true)
                .defaultPage(mCurrentPage)
                .load();
    }

}
