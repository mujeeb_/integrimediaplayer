package com.integrimarine.integrimediaplayer.fonts;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;


public class RajdhaniMedium extends android.support.v7.widget.AppCompatTextView {
    public RajdhaniMedium(Context context, AttributeSet attrs) {
        super(context, attrs);
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/Rajdhani-Medium.ttf");
        this.setTypeface(tf);
    }
}