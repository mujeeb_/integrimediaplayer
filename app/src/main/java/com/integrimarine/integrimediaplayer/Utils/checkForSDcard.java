package com.integrimarine.integrimediaplayer.Utils;

import android.os.Environment;

public class checkForSDcard {
    public boolean isSDCardPresent() {
        if (Environment.getExternalStorageState().equals(

                Environment.MEDIA_MOUNTED)) {
            return true;
        }
        return false;
    }
}
