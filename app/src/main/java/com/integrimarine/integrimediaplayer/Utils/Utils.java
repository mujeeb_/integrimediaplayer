package com.integrimarine.integrimediaplayer.Utils;

import android.os.Environment;

import java.io.File;

public class Utils {
    public static final String downloadDirectory = "ShipVideo";
     public static final String extensionForVideo[] = {".mp4",".mkv",".webm",".flv",".avi",".yuv",".mpeg"};
    public static final String extensionForImage[] = {".png",".jpg",".jpeg"};
    public static final String extensionForPDF = ".pdf";

    public static String _BASE_PATH = Environment.getExternalStorageDirectory().getAbsolutePath()+"/shipVideo";
}
