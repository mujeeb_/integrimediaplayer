package com.integrimarine.integrimediaplayer.Activities;

import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.WindowManager;

import com.integrimarine.integriclient.Database.DatabaseSingleton;
import com.integrimarine.integrimediaplayer.Database.Models.CUMediaFileModel;
import com.integrimarine.integrimediaplayer.Fragments.ImageFragment;
import com.integrimarine.integrimediaplayer.Fragments.PdfFragment;
import com.integrimarine.integrimediaplayer.Fragments.VideoFragment;
import com.integrimarine.integrimediaplayer.R;

import java.util.Date;

public class TvScreenActivity extends AppCompatActivity {


    private FragmentManager fragmentManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tv_screen);
        /**
         * To View Full Screen
         */
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        /**
         * Fill Each Framelayout with Fragment Container
         */
        fragmentManager = getSupportFragmentManager();

        fragmentManager.beginTransaction().add(R.id.activity_screen_video_view, new VideoFragment()).commit();

        fragmentManager.beginTransaction().add(R.id.activity_screen_image_view, new ImageFragment()).commit();

        fragmentManager.beginTransaction().add(R.id.activity_screen_pdf_view, new PdfFragment()).commit();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

}
