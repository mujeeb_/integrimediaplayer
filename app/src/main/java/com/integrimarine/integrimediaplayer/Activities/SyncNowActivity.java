package com.integrimarine.integrimediaplayer.Activities;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.sqlite.SQLiteConstraintException;
import android.net.Uri;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.text.format.Formatter;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.util.Util;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.integrimarine.integriclient.Database.DatabaseSingleton;
import com.integrimarine.integrimediaplayer.Config.Config;
import com.integrimarine.integrimediaplayer.Database.Models.CUMediaFileModel;
import com.integrimarine.integrimediaplayer.Database.Models.CUMediaScreenScheduleModel;
import com.integrimarine.integrimediaplayer.R;
import com.integrimarine.integrimediaplayer.Utils.Utils;
import com.integrimarine.integrimediaplayer.Utils.WifiUtil;
import com.integrimarine.integrimediaplayer.Utils.checkForSDcard;
import com.integrimarine.integrimediaplayer.fonts.RajdhaniMedium;

import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.Socket;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Calendar;
import java.util.Enumeration;
import java.util.List;
import java.util.Locale;

public class SyncNowActivity extends AppCompatActivity {

    private static final String TAG = "INTEGRI";
    RajdhaniMedium EnableWifiAndSync, lastSyncTime, lastSyncDate;
    ImageView tvScreen;
    RecyclerView recyclerView;
    Context mContext;
    ProgressDialog mProgressDialog;
    CardView recieveNow;
    WifiManager wifiManager;
    String HostAddress;
    String outputPath;
    String fileLength;
    int fileSize = 0;
    List<CUMediaFileModel> mMediaFileModels;
    List<CUMediaScreenScheduleModel> mMediaScreenScheduleModels;

    File storage = null;
    Dialog dialog;
    /**
     * Runtime Permission
     */
    int ALL_PERMISSIONS = 101;
    final String[] permissions = new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE};

    static final int SocketServerPORT = 8888;
    private String HOST_IP_ADDRESS = "192.168.43.1";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sync_now);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        ActivityCompat.requestPermissions(this, permissions, ALL_PERMISSIONS);

        //Get File if SD card is present
        if (new checkForSDcard().isSDCardPresent()) {

            storage = new File(Utils._BASE_PATH);
        } else Toast.makeText(mContext, "Oops!! There is no SD Card.", Toast.LENGTH_SHORT).show();
        if (!storage.exists()) {
            storage.mkdir();
            Log.d("Created: ", "created");
        }

        EnableWifiAndSync = findViewById(R.id.syncNowOnWifi);
        lastSyncDate = findViewById(R.id.lastSyncDate);
        lastSyncTime = findViewById(R.id.lastSyncTime);
        recieveNow = findViewById(R.id.recieveFiles);
        tvScreen = findViewById(R.id.tvsScreen);
        disableHospot();
        mContext = this;

        EnableWifiAndSync.setText("Click here to sync");

        //stored previos sync
        SharedPreferences prefs = mContext.getSharedPreferences("LastSyncTimeDate", MODE_PRIVATE);
        String time = prefs.getString("Time", null);
        String date = prefs.getString("Date", null);


        if (time != null) {
            lastSyncTime.setText("Last Sync time :" + time);
            lastSyncDate.setText("Last Sync Date :" + date);
        }

        File storage = null;
        //Get File if SD card is present
        if (new checkForSDcard().isSDCardPresent()) {

            storage = new File(Utils._BASE_PATH);
        } else Toast.makeText(mContext, "Oops!! There is no SD Card.", Toast.LENGTH_SHORT).show();
        if (!storage.exists()) {
            storage.mkdir();
            Log.d("Created: ", "creted");
        }

        wifiManager = (WifiManager) mContext.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        ///Disables Hotspot and connect wifo
        if (checkSystemWritePermission()) {
            wifiManager.setWifiEnabled(false);
            disableHospot();
        }
        recieveNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                wifiManager.setWifiEnabled(true);
                enableWifi();

            }
        });
        tvScreen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(SyncNowActivity.this, TvScreenActivity.class));
            }
        });


    }


    private class ClientRxThread extends AsyncTask<String, Integer, String> {
        String distinationAddress;
        int dstPort;
        boolean hasError = false;

        ClientRxThread(String address, int port) {
            distinationAddress = address;
            dstPort = port;
        }

        protected void onPreExecute() {
            super.onPreExecute();
            if (mContext != null) ((Activity) mContext).runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mProgressDialog = new ProgressDialog(SyncNowActivity.this);
                    mProgressDialog.setMessage("Recieving Files");
                    mProgressDialog.setIndeterminate(false);
                    mProgressDialog.setCancelable(false);
                    mProgressDialog.setCanceledOnTouchOutside(false);
                    mProgressDialog.setMax(100);
                    mProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                    mProgressDialog.show();
                }
            });
        }

        @Override
        protected void onProgressUpdate(final Integer... values) {
            super.onProgressUpdate(values);
            if (mContext != null) ((Activity) mContext).runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mProgressDialog.setProgress(values[0]);
                }
            });
        }

        @Override
        protected String doInBackground(String... strings) {
            Socket socket = null;

            try {
                ((Activity) mContext).runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        Toast.makeText(mContext, "HOST: " + HostAddress + " , PORT: " + dstPort, Toast.LENGTH_LONG).show();
                    }
                });
                socket = new Socket(HOST_IP_ADDRESS, dstPort);


                int count;
                byte[] buffer = new byte[8192]; // or 4096, or more

                InputStream inputStream = socket.getInputStream();
                DataInputStream dataInputStream = new DataInputStream(socket.getInputStream());

                String mediaScreenSchedules = dataInputStream.readUTF();
                String mediaFiles = dataInputStream.readUTF();
                Log.e("doInBackground: ", mediaFiles);
                Log.e("doInBackground: ", mediaScreenSchedules);

                Gson gson = new Gson();
                mMediaFileModels = gson.fromJson(mediaFiles, new TypeToken<List<CUMediaFileModel>>() {
                }.getType());
                mMediaScreenScheduleModels = gson.fromJson(mediaScreenSchedules, new TypeToken<List<CUMediaScreenScheduleModel>>() {
                }.getType());
                for (CUMediaScreenScheduleModel cuMediaScreenModel : mMediaScreenScheduleModels) {
                    try {
                        Log.e("doInBackground: ", cuMediaScreenModel.getRef_customer_id());
                        DatabaseSingleton.getInstance(mContext).getDatabaseInterface().cu_media_screen_schedules().insert(cuMediaScreenModel);
                    } catch (SQLiteConstraintException exception) {
                        DatabaseSingleton.getInstance(mContext).getDatabaseInterface().cu_media_screen_schedules().update(cuMediaScreenModel);
                    }
                }
                for (CUMediaFileModel cuMediaFileModel : mMediaFileModels) {
                    try {
                        DatabaseSingleton.getInstance(mContext).getDatabaseInterface().cu_media_files().insert(cuMediaFileModel);
                    } catch (SQLiteConstraintException exception) {
                        DatabaseSingleton.getInstance(mContext).getDatabaseInterface().cu_media_files().update(cuMediaFileModel);
                    }
                }
                String Size = dataInputStream.readUTF();
                Log.d("size: ", Size);
                fileSize = Integer.parseInt(Size);

                for (int x = 0; x < fileSize; x++) {
                    Log.d("doInBackground: ", x + "");
                    long total = 0;
                    outputPath = dataInputStream.readUTF();
                    Log.d("out: ", outputPath + "");

                    fileLength = dataInputStream.readUTF();
                    Log.d("fileLength: ", fileLength + "");

                    File file = new File(Utils._BASE_PATH, outputPath + "");
                    Log.d("file Path: ", file + "");

                    FileOutputStream fileOutputStream = new FileOutputStream(file);
                    BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(fileOutputStream);

                    Long fileLengthLong = Long.parseLong(fileLength);
                    //Actual file size of Each file
                    Long fileSize = fileLengthLong;

//                    while ((count = is.read( buffer )) > 0) {
//                        total += count;
//                        publishProgress( (int) (total * 100 / fileLengthLong) );
//                        bos.write( buffer, 0, count );
//
//                    }
                    while (fileLengthLong > 0 && (count = inputStream.read(buffer, 0, (int) Math.min(buffer.length, fileLengthLong))) != -1) {
                        total += count;
                        publishProgress((int) (total * 100 / fileSize));
                        bufferedOutputStream.write(buffer, 0, count);
                        fileLengthLong -= count;
                    }
                    bufferedOutputStream.close();
                    String success = dataInputStream.readUTF();
                    Log.d("success: ", success);
                }
                dataInputStream.close();
                socket.close();

                ((Activity) mContext).runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        Toast.makeText(mContext, "Finished", Toast.LENGTH_LONG).show();
                    }
                });

            } catch (IOException e) {
                hasError = true;
                e.printStackTrace();
                Writer writer = new StringWriter();
                e.printStackTrace(new PrintWriter(writer));
                //enableWifi( SSID );
                final String eMsg = "Something wrong: error log: " + writer.toString();
                ((Activity) mContext).runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        mProgressDialog.dismiss();
                        EnableWifiAndSync.setText("retry to Sync...");
                        Toast.makeText(SyncNowActivity.this, "Please connect to integri wifi manually and try again", Toast.LENGTH_LONG).show();
                    }
                });

            } finally {
                if (socket != null) {
                    try {
                        socket.close();
                    } catch (IOException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (!hasError)
                ((Activity) mContext).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mProgressDialog.dismiss();
                        EnableWifiAndSync.setText("Sync Completed");
                        Calendar calendar = Calendar.getInstance(Locale.getDefault());
                        int hour = calendar.get(Calendar.HOUR_OF_DAY);
                        int minute = calendar.get(Calendar.MINUTE);
                        int day = calendar.get(Calendar.DAY_OF_MONTH);
                        int month = calendar.get(Calendar.MONTH);
                        int year = calendar.get(Calendar.YEAR);
                        Log.d("Month da: ", month + "");
                        String TimeAmPm;
                        if (hour <= 12) {
                            TimeAmPm = "AM";
                        } else {
                            TimeAmPm = "PM";
                        }
                        lastSyncTime.setText("Last Sync Time : " + hour % 12 + ":" + minute + " " + TimeAmPm);
                        lastSyncDate.setText("Last Sync Date : " + day + "/" + month + 1 + "/" + year);
                        String Time = hour % 12 +":" + minute + " " + TimeAmPm;
                        String OnDate = day + "/" + month+1 + "/" + year;
                        SharedPreferences.Editor editor = mContext.getSharedPreferences("LastSyncTimeDate", MODE_PRIVATE).edit();
                        editor.putString("Time", Time + "");
                        editor.putString("Date", OnDate + "");
                        editor.apply();
                    }
                });
        }
    }


    private void disableHospot() {
        try {

            WifiManager wifiManager = (WifiManager) getApplicationContext().getSystemService(WIFI_SERVICE);
            WifiConfiguration wifi_configuration = new WifiConfiguration();
            //USE REFLECTION TO GET METHOD "SetWifiAPEnabled"
            Method method = wifiManager.getClass().getMethod("setWifiApEnabled", WifiConfiguration.class, boolean.class);
            method.invoke(wifiManager, wifi_configuration, false);
        } catch (NoSuchMethodException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IllegalArgumentException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }


    }

    private boolean checkSystemWritePermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (Settings.System.canWrite(mContext)) return true;
            else openAndroidPermissionsMenu();
        }
        return false;
    }

    private void openAndroidPermissionsMenu() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            Intent intent = new Intent(Settings.ACTION_MANAGE_WRITE_SETTINGS);
            intent.setData(Uri.parse("package:" + mContext.getPackageName()));
            mContext.startActivity(intent);
        }
    }


    public void enableWifi() {

        final WifiManager wifiManager = (WifiManager) mContext.getApplicationContext().getSystemService(mContext.WIFI_SERVICE);
        wifiManager.disconnect();
        WifiConfiguration conf = new WifiConfiguration();
        conf.SSID = "\"" + Config.SSID + "\"";
        //conf.wepKeys[0] = "\"" + Config.SSID_PWD + "\"";

        conf.wepTxKeyIndex = 0;
        conf.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.NONE);
        conf.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.CCMP);
       // conf.preSharedKey = "\""+ Config.SSID_PWD +"\"";
        wifiManager.addNetwork(conf);


        List<WifiConfiguration> list = wifiManager.getConfiguredNetworks();

        for (WifiConfiguration i : list) {
            if (i.SSID != null && i.SSID.equals("\"" + Config.SSID + "\"")) {
                wifiManager.disconnect();
                wifiManager.enableNetwork(i.networkId, true);
                wifiManager.reconnect();
                intToInetAddress(wifiManager.getDhcpInfo().serverAddress);

                WifiInfo wifi_inf = wifiManager.getConnectionInfo();
                ((Activity) mContext).runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        EnableWifiAndSync.setText("Connecting...,please wait");
                    }
                });
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            String ip = WifiUtil.getIPAddress(true);
                            HOST_IP_ADDRESS = ip.replaceAll("\\.\\d+$", ".1");
                            ;
                            Log.d(TAG, ip);
                        } catch (Exception ex) {
                            Log.d(TAG, ex.toString());
                        }
                        ClientRxThread clientRxThread = new ClientRxThread(HostAddress, SocketServerPORT);
                        clientRxThread.execute();
                    }
                }, 10000);


                break;
            }
        }

    }

    public InetAddress intToInetAddress(int hostAddress) {
        byte[] addressBytes = {(byte) (0xff & hostAddress), (byte) (0xff & (hostAddress >> 8)), (byte) (0xff & (hostAddress >> 16)), (byte) (0xff & (hostAddress >> 24))};
        try {
            HostAddress = String.valueOf(InetAddress.getByAddress(addressBytes));
            HostAddress = HostAddress.substring(1);
            Log.d("intToInetAddress: ", HostAddress + "");

        } catch (UnknownHostException e) {
            e.printStackTrace();
        }

        try {
            return InetAddress.getByAddress(addressBytes);

        } catch (UnknownHostException e) {
            throw new AssertionError();
        }
    }

    private void openSettings() {
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", getPackageName(), null);
        intent.setData(uri);
        startActivityForResult(intent, 101);
    }

    /**
     * On RequestPermissionsResult to ask permission and to check permission granted or not
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        for (String permission : permissions) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, permission)) {

            } else {
                if (ActivityCompat.checkSelfPermission(this, permission) == PackageManager.PERMISSION_GRANTED) {

                } else {
                }
            }
        }
    }

    boolean checkForPermissions() {
        boolean hasPermissions = true;
        for (String permission : permissions) {
            if (ContextCompat.checkSelfPermission(this, permission) != PackageManager.PERMISSION_GRANTED) {
                hasPermissions = false;
                ActivityCompat.requestPermissions(this, permissions, ALL_PERMISSIONS);
                break;
            }
        }
        return hasPermissions;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        wifiManager.setWifiEnabled(false);
        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (dialog != null) dialog.dismiss();
        dialog = null;
    }
}
