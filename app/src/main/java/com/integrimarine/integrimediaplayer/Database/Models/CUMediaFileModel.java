package com.integrimarine.integrimediaplayer.Database.Models;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

@Dao
@Entity(tableName = "cu_media_files")
public class CUMediaFileModel {

    @PrimaryKey @NonNull
    @ColumnInfo(name = "media_file_id")
    private String media_file_id;

    @ColumnInfo(name = "ref_customer_id")
    private String ref_customer_id;

    @ColumnInfo(name = "media_name")
    private String media_name;

    @ColumnInfo(name = "media_type")
    private String media_type;

    @ColumnInfo(name = "url")
    private String url;

    @ColumnInfo(name = "is_deleted")
    private Boolean is_deleted;

    public String getMedia_file_id() {
        return media_file_id;
    }

    public void setMedia_file_id(String media_file_id) {
        this.media_file_id = media_file_id;
    }

    public String getRef_customer_id() {
        return ref_customer_id;
    }

    public void setRef_customer_id(String ref_customer_id) {
        this.ref_customer_id = ref_customer_id;
    }

    public String getMedia_name() {
        return media_name;
    }

    public void setMedia_name(String media_name) {
        this.media_name = media_name;
    }

    public String getMedia_type() {
        return media_type;
    }

    public void setMedia_type(String media_type) {
        this.media_type = media_type;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Boolean getIs_deleted() {
        return is_deleted;
    }

    public void setIs_deleted(Boolean is_deleted) {
        this.is_deleted = is_deleted;
    }


}
