package com.integrimarine.integrimediaplayer.Database.Models;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.TypeConverters;
import android.support.annotation.NonNull;

import com.integrimarine.integriclient.Database.Util.DatatypeConverter;

@Entity(tableName = "cu_media_screens")
@TypeConverters(DatatypeConverter.class)
@Dao
public class CUMediaScreenModel {

    @PrimaryKey @NonNull
    @ColumnInfo(name = "media_screen_id")
    private String media_screen_id;

    @ColumnInfo(name = "ref_customer_id")
    private String ref_customer_id;

    @ColumnInfo(name = "ref_ship_id")
    private String ref_ship_id;

    @ColumnInfo(name = "name")
    private String name;

    @ColumnInfo(name = "is_deleted")
    private Boolean is_deleted;


    public String getMedia_screen_id() {
        return media_screen_id;
    }

    public void setMedia_screen_id(String media_screen_id) {
        this.media_screen_id = media_screen_id;
    }

    public String getRef_customer_id() {
        return ref_customer_id;
    }

    public void setRef_customer_id(String ref_customer_id) {
        this.ref_customer_id = ref_customer_id;
    }

    public String getRef_ship_id() {
        return ref_ship_id;
    }

    public void setRef_ship_id(String ref_ship_id) {
        this.ref_ship_id = ref_ship_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getIs_deleted() {
        return is_deleted;
    }

    public void setIs_deleted(Boolean is_deleted) {
        this.is_deleted = is_deleted;
    }

}
