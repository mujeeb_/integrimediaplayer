package com.integrimarine.integrimediaplayer.Database.Interfaces;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.integrimarine.integrimediaplayer.Database.Models.CUMediaFileModel;

import java.util.List;

@Dao
public interface CUMediaFileInterface {

    @Query( "SELECT * FROM cu_media_files" )
    public abstract List<CUMediaFileModel> getAllMediaFiles();

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    public long insert(CUMediaFileModel mediaFileModel);

    @Update(onConflict = OnConflictStrategy.IGNORE)
    public void update(CUMediaFileModel mediaFileModel);


}
