package com.integrimarine.integriclient.Database;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.TypeConverters;

import com.integrimarine.integriclient.Database.Util.DatatypeConverter;
import com.integrimarine.integrimediaplayer.Database.Interfaces.CUMediaFileInterface;
import com.integrimarine.integrimediaplayer.Database.Interfaces.CUMediaScreenInterface;
import com.integrimarine.integrimediaplayer.Database.Interfaces.CUMediaScreenScheduleInterface;
import com.integrimarine.integrimediaplayer.Database.Models.CUMediaFileModel;
import com.integrimarine.integrimediaplayer.Database.Models.CUMediaScreenModel;
import com.integrimarine.integrimediaplayer.Database.Models.CUMediaScreenScheduleModel;

@Database(entities = {CUMediaScreenModel.class, CUMediaFileModel.class, CUMediaScreenScheduleModel.class}, version = 1, exportSchema = false)
public abstract class DatabaseInterface extends RoomDatabase {

    @TypeConverters(DatatypeConverter.class)
    public abstract CUMediaScreenInterface cu_media_screens();

    @TypeConverters(DatatypeConverter.class)
    public abstract CUMediaFileInterface cu_media_files();

    @TypeConverters(DatatypeConverter.class)
    public abstract CUMediaScreenScheduleInterface cu_media_screen_schedules();

}
