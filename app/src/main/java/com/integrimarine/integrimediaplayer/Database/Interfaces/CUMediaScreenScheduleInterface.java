package com.integrimarine.integrimediaplayer.Database.Interfaces;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;


import com.integrimarine.integrimediaplayer.Database.Models.CUMediaFileModel;
import com.integrimarine.integrimediaplayer.Database.Models.CUMediaScreenScheduleModel;

import java.util.List;

@Dao
public interface CUMediaScreenScheduleInterface {

    @Query( "SELECT * FROM cu_media_screen_schedules" )
    public abstract List<CUMediaScreenScheduleModel> getAllScreenSchedules();

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    public long insert(CUMediaScreenScheduleModel screenScheduleModel);

    @Update(onConflict = OnConflictStrategy.IGNORE)
    public void update(CUMediaScreenScheduleModel screenScheduleModel);

    @Query( "SELECT * FROM cu_media_screen_schedules" )
    public abstract List<CUMediaScreenScheduleModel> getAllMediaScreenSchudules();

    @Query("select * from cu_media_files where media_file_id = ( select ref_media_file_id FROM cu_media_screen_schedules where screen_type= :screen_type and  schedule_start_date <  :time and schedule_end_date > :time limit 1) limit 1")
    //@Query( "SELECT (select url from cu_media_files where media_file_id = ref_media_file_id) FROM cu_media_screen_schedules where screen_type  = :screen_type AND schedule_start_date > :time AND schedule_end_date < :time LIMIT 1" )
    public abstract CUMediaFileModel findMediaScheduleByScreenType(String screen_type, long time);
}
